package com.example.testtasks;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestTasksApplication {

    public static void main(String[] args) {
        firstTask();
        secondTask(4,2);
        thirdTask("testy, test, testy test test test test testo");
    }

    private static void firstTask() {
        StringBuilder strings = new StringBuilder();
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                if (i % 7 == 0) {
                    strings.append("TwoSeven ");
                } else {
                    strings.append("Two ");
                }
            } else {
                if (i % 7 == 0) {
                    strings.append("Seven ");
                } else {
                    strings.append(String.valueOf(i)).append(" ");
                }
            }
        }
        System.out.println("firstTask: " + strings);
    }

    private static void secondTask(long m, long r) {
        if (r > m || r < 0 || m < 0) throw new IllegalArgumentException("invalid inputs r or m");
        long factM = factorial(m);
        long factR = factorial(r);
        long factDiff = factorial(m - r);
        //result of a combination is always natural (no need to cast to double)
        System.out.println("secondTask: " + factM / (factR * factDiff));
    }

    private static long factorial(long n) {
        long result = 1;
        for (long i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    private static void thirdTask(String text) {
        Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher matcher = p.matcher(text);
        Map<String, Integer> wordsAndCounts = new HashMap<>();
        while (matcher.find()) {
            String word = matcher.group();
            wordsAndCounts.merge(word, 1, (a, b) -> a + b);
        }
        System.out.println("thirdTask:");
        wordsAndCounts.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(System.out::println);
    }

}
